set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR mips64el)
set(TRIPLE "mips64el-linux-gnuabi64")

set(CMAKE_C_COMPILER "${TRIPLE}-gcc")
set(CMAKE_CXX_COMPILER "${TRIPLE}-g++")
set(CMAKE_ASM_COMPILER "${TRIPLE}-as")
set(CMAKE_AR "${TRIPLE}-ar")
set(CMAKE_NM "${TRIPLE}-nm")
set(CMAKE_STRIP "${TRIPLE}-strip")
set(CMAKE_RANLIB "${TRIPLE}-ranlib")
set(CMAKE_LINKER "${TRIPLE}-ld")
set(CMAKE_OBJCOPY "${TRIPLE}-objcopy")
set(CMAKE_READELF "${TRIPLE}-readelf")

# where is the target environment located
set(CMAKE_FIND_ROOT_PATH /usr/${TRIPLE})

# adjust the default behavior of the FIND_XXX() commands:
# search programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# search headers and libraries in the target environment
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)
